<!DOCTYPE html>
<html>
<head>
<title>Form Register</title>
</head>
<body>
<h1>Buat Account Baru!</h1>
<h3>Sign Up Form</h3>
<form action="/welcome" method="POST">
@csrf
<p>First name:</p>
    <input type="text" name="firstname">

<p>Last name:</p>
    <input type="text" name="lastname">

<p>Gender:</p>
    <input type="radio" name="gender" value="male"> Male
    <br>
    <input type="radio" name="gender" value="female"> Female
    <br>
    <input type="radio" name="gender" value="othergender"> Other

<p>Nasionality:</p>
    <select name="nasionality">
        <option value="indonesian">Indonesian</option>
        <option value="singaporean">Singaporean</option>
        <option value="malaysian">Malaysian</option>
        <option value="australian">Australian</option>
    </select>

<p>Language Spoken:</p>
    <input type="checkbox" name="bahasa"> Bahasa Indonesian
    <br>   
    <input type="checkbox" name="english"> English
    <br>
    <input type="checkbox" name="otherlanguage"> Other

<p>Bio:</p>
    <textarea name="bio" rows="10" cols="30"></textarea>
    <br>

<input type="submit" value="Sign Up">
</form>
</body>
</html>